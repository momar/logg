package logg

type Output interface {
	Message(*Message)
}

type outputs struct {
	Console consoleOutput
}

var Outputs = outputs{}
