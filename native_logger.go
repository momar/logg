package logg

import (
	"fmt"
	"io"
	"os"
)

type NativeLogger struct {
	tagCount int
	logger   *boundLogger
}

func (l *NativeLogger) Fatal(v ...interface{}) {
	l.logger.Error(fmt.Sprint(v...))
	os.Exit(1)
}
func (l *NativeLogger) Fatalf(format string, v ...interface{}) {
	l.logger.Error(format, v...)
	os.Exit(1)
}
func (l *NativeLogger) Fatalln(v ...interface{}) {
	l.Fatal(v...)
}

func (l *NativeLogger) Panic(v ...interface{}) {
	formatted := fmt.Sprint(v...)
	l.logger.Error(formatted)
	panic(formatted)
}
func (l *NativeLogger) Panicf(format string, v ...interface{}) {
	l.logger.Error(format, v...)
	panic(fmt.Sprint(v...))
}
func (l *NativeLogger) Panicln(v ...interface{}) {
	l.Panic(v...)
}

func (l *NativeLogger) Print(v ...interface{}) {
	l.logger.Info(fmt.Sprint(v...))
}
func (l *NativeLogger) Printf(format string, v ...interface{}) {
	l.logger.Info(format, v...)
}
func (l *NativeLogger) Println(v ...interface{}) {
	l.Print(v...)
}

func (l *NativeLogger) Output(calldepth int, s string) error {
	l.Print(s)
	return nil
}

func (l *NativeLogger) Flags() int {
	switch v := l.logger.fields["flags"].(type) {
	case int:
		return v
	default:
		return 0
	}
}
func (l *NativeLogger) SetFlags(flag int) {
	if flag != 0 {
		l.logger.fields["flags"] = flag
	} else if _, ok := l.logger.fields["flags"]; ok {
		delete(l.logger.fields, "flags")
	}
}

func (l *NativeLogger) Prefix() string {
	if len(l.logger.tags) > l.tagCount {
		return l.logger.tags[l.tagCount]
	}
	return ""
}
func (l *NativeLogger) SetPrefix(prefix string) {
	if prefix == "" {
		l.logger.tags = l.logger.tags[:l.tagCount]
	} else if len(l.logger.tags) > l.tagCount {
		l.logger.tags[l.tagCount] = prefix
	} else {
		l.logger.tags = append(l.logger.tags, prefix)
	}
}

type nativeWriter struct {
	l *NativeLogger
}

func (w nativeWriter) Write(p []byte) (n int, err error) {
	w.l.logger.Debug("%s", p)
	return len(p), nil
}

func (l *NativeLogger) Writer() io.Writer {
	return nativeWriter{l}
}
func (l *NativeLogger) SetOutput(w io.Writer) {
	l.logger.Tag("logg").Warn("Ignored call to *NativeLogger.SetOutput(io.Writer)")
}
