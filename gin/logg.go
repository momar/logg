package logg4gin

import (
	"fmt"
	"strings"

	"codeberg.org/momar/logg"
	"github.com/gin-gonic/gin"
)

type w struct {
	w func(string)
}

func (w w) Write(p []byte) (n int, err error) {
	w.w(string(p))
	return len(p), nil
}

var DefaultWriter = w{func(x string) {
	x = strings.TrimSuffix(strings.TrimSuffix(strings.TrimPrefix(x, "[GIN-debug] "), "\n"), "\n")
	if strings.HasPrefix(x, "[WARNING] ") {
		x = strings.TrimPrefix(x, "[WARNING] ")
		logg.Tag("gin").Warn("%s", x)
	} else if strings.HasPrefix(x, "Listening and serving ") {
		logg.Tag("gin").Info("%s", x)
	} else {
		logg.Tag("gin").Debug("%s", x)
	}
}}
var DefaultErrorWriter = w{func(x string) {
	logg.Tag("gin").Error("%s", x)
}}
var DebugPrintRouteFunc = func(httpMethod, absolutePath, handlerName string, nuHandlers int) {
	logg.Tag("gin", "setup").Field("handlers", nuHandlers).Debug("%s%s%v\033[0;1m %v \033[0m-> %v",
		strings.Repeat(" ", 7-len(httpMethod)), getMethodColor(httpMethod), httpMethod,
		absolutePath,
		handlerName,
	)
}
var LogFormatter = func(param gin.LogFormatterParams) string {
	err := ""
	if param.ErrorMessage != "" {
		err = "\n\033[31;1m" + param.ErrorMessage
	}
	return fmt.Sprintf("%s%s%v\033[0;1m %v \033[0m-> %v %s%v  \033[1;35mtime=%v%s\033[0m",
		strings.Repeat(" ", 7-len(param.Method)), getMethodColor(param.Method), param.Method,
		param.Path,
		param.Request.Proto, getStatusColor(param.StatusCode), param.StatusCode, param.Latency,
		err,
	)
}

func getMethodColor(httpMethod string) string {
	methodColor := map[string]string{
		"GET":     "\033[34;1m",
		"POST":    "\033[32;1m",
		"PUT":     "\033[33;1m",
		"DELETE":  "\033[31;1m",
		"PATCH":   "\033[35;1m",
		"HEAD":    "\033[36;1m",
		"OPTIONS": "\033[36;1m",
	}[httpMethod]
	if methodColor == "" {
		methodColor = "\033[30;1m"
	}
	return methodColor
}
func getStatusColor(code int) string {
	switch {
	case code < 200:
		return "\033[35;1m"
	case code >= 200 && code < 300:
		return "\033[32;1m"
	case code >= 300 && code < 400:
		return "\033[36;1m"
	case code >= 400 && code < 500:
		return "\033[33;1m"
	default:
		return "\033[31;1m"
	}
}
