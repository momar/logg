package logg

import (
	"fmt"
	"os"
	"regexp"
	"strings"
)

type consoleOutput struct{}

var consoleColor = regexp.MustCompile(`\[[^\]]\]`)
var consoleOptional = regexp.MustCompile(`\{\}`)
var consoleVariable = regexp.MustCompile(`$\d*[><^]?.`)
var consoleEscapeSequence = regexp.MustCompile("\033\\[[^m]*m")

var DefaultConsoleFormatter = func(m *Message) string {
	// Time
	var t string
	if Options.UTC {
		t = m.Time.UTC().Format(Options.Time)
	} else {
		t = m.Time.Format(Options.Time)
	}
	c, _ := Options.ColorMap["time"]
	str := c + t + Colors.FormatReset + " "

	// Level
	var l = string(m.Level)
	for len(l) < 5 {
		l = " " + l
	}
	c, _ = Options.ColorMap["L"+string(m.Level)]
	str += c + l + ":" + Colors.FormatReset + " "
	
	// Tags
	t = strings.Join(m.Tags, "/")
	if len(t) > 0 {
		c, _ = Options.ColorMap["tags"]
		str += c + "[" + t + "]" + Colors.FormatReset + " "
	}

	// Message
	str += m.Message

	// Fields
	var f string
	for k, v := range m.Fields {
		f += fmt.Sprintf(" %s=%+v", k, v)
	}
	if len(f) > 0 {
		c, _ = Options.ColorMap["fields"]
		str += "  " + c + f[1:] + Colors.FormatReset
	}

	// Caller
	if len(m.Caller) > 0 {
		c, _ = Options.ColorMap["caller"]
		str += "  " + c + m.Caller
	}

	if len(f) > 0 || len(m.Caller) > 0 {
		str += Colors.FormatReset
	}

	// Strip colors
	if !Options.Colors {
		str = consoleEscapeSequence.ReplaceAllString(str, "")
	}
	return str
}

func (consoleOutput) Message(m *Message) {
	str := Options.Format(m)

	if m.Level == LevelInfo || m.Level == LevelWarn || (Options.Debug && m.Level == LevelDebug) {
		os.Stdout.WriteString(str + "\n")
	} else if m.Level == LevelError {
		os.Stderr.WriteString(str + "\n")
	}
}
