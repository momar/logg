package logg

import (
	"fmt"
	"time"
)

var defaultLogger = &boundLogger{
	tags:   []string{},
	fields: map[string]interface{}{},
}

func Error(f string, v ...interface{}) {
	defaultLogger.Error(f, v...)
}

func Warn(f string, v ...interface{}) {
	defaultLogger.Warn(f, v...)
}

func Info(f string, v ...interface{}) {
	defaultLogger.Info(f, v...)
}

func Debug(f string, v ...interface{}) {
	defaultLogger.Debug(f, v...)
}

func Tag(tag ...string) *boundLogger {
	return defaultLogger.Tag(tag...)
}

func Field(name string, value interface{}) *boundLogger {
	return defaultLogger.Field(name, value)
}

func Fields(fields map[string]interface{}) *boundLogger {
	return defaultLogger.Fields(fields)
}

func Logger() *NativeLogger {
	return defaultLogger.Logger()
}

type boundLogger struct {
	tags   []string
	fields map[string]interface{}
}

func (l *boundLogger) Error(f string, v ...interface{}) {
	log(&Message{Message: fmt.Sprintf(f, v...), Fields: l.fields, Tags: l.tags, Level: LevelError})
}

func (l *boundLogger) Warn(f string, v ...interface{}) {
	log(&Message{Message: fmt.Sprintf(f, v...), Fields: l.fields, Tags: l.tags, Level: LevelWarn})
}

func (l *boundLogger) Info(f string, v ...interface{}) {
	log(&Message{Message: fmt.Sprintf(f, v...), Fields: l.fields, Tags: l.tags, Level: LevelInfo})
}

func (l *boundLogger) Debug(f string, v ...interface{}) {
	log(&Message{Message: fmt.Sprintf(f, v...), Fields: l.fields, Tags: l.tags, Level: LevelDebug})
}

func (l *boundLogger) Tag(tag ...string) *boundLogger {
	r := l.copy()
	r.tags = append(r.tags, tag...)
	return r
}

func (l *boundLogger) Field(name string, value interface{}) *boundLogger {
	r := l.copy()
	r.fields = map[string]interface{}{}
	for k, v := range l.fields {
		r.fields[k] = v
	}
	r.fields[name] = value
	return r
}

func (l *boundLogger) Fields(fields map[string]interface{}) *boundLogger {
	r := l.copy()
	r.fields = map[string]interface{}{}
	for k, v := range l.fields {
		r.fields[k] = v
	}
	for k, v := range fields {
		fields[k] = v
	}
	return r
}

func (l *boundLogger) Logger() *NativeLogger {
	return &NativeLogger{logger: l, tagCount: len(l.tags)}
}

func log(m *Message) {
	m.Time = time.Now()
	if Options.Debug {
		m.Caller = getCaller()
	}

	for _, o := range Options.Output {
		o.Message(m)
	}
}

func (l *boundLogger) copy() *boundLogger {
	fields := map[string]interface{}{}
	for k, v := range l.fields {
		fields[k] = v
	}
	return &boundLogger{
		tags:   append([]string{}, l.tags...),
		fields: l.fields,
	}
}
