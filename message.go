package logg

import "time"

type level string

const (
	LevelError level = "error"
	LevelWarn  level = "warn"
	LevelInfo  level = "info"
	LevelDebug level = "debug"
)

type Message struct {
	Time    time.Time
	Level   level
	Message string
	Fields  map[string]interface{}
	Tags    []string
	Caller  string
}
