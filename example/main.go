package main

import "codeberg.org/momar/logg"

func main() {
	logg.Debug("This only shows up if the DEBUG environment variable is set")
	logg.Info("Hello %s", "World!")
	logg.Warn("Something went a little bit wrong")
	logg.Error("Something went " + logg.Colors.Red + "very" + logg.Colors.FormatReset + " wrong")

	l := logg.Tag("hello")
	l.Tag("world").Info("Hello World")

	l.Field("test", true).Field("whatever", false).Info("Hi")
	l.Fields(map[string]interface{}{"hello": "world"}).Info("Whatever")
}
