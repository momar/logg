module codeberg.org/momar/logg

go 1.12

require (
	codeberg.org/momar/ternary v0.0.0-20190206091215-7d184a388958
	github.com/gin-gonic/gin v1.4.0
)
