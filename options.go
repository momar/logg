package logg

import (
	"os"

	"codeberg.org/momar/ternary"
)

type options struct {
	Debug    bool
	Output   []Output
	Colors   bool
	ColorMap map[string]string
	UTC      bool
	Time     string
	Format   func(m *Message) string
}

var Options = options{
	Debug: ternary.If(os.Getenv("DEBUG") != "", true, false).(bool),
	Output: []Output{
		Outputs.Console,
	},
	Colors: true,
	ColorMap: map[string]string{
		"Lwarn":  Colors.FormatBold + Colors.Yellow,
		"Lerror": Colors.FormatBold + Colors.Red,
		"Linfo":  Colors.FormatBold + Colors.Blue,
		"Ldebug": Colors.FormatBold + Colors.Magenta,
		"time":   Colors.FormatBold + Colors.Black,
		"tags":   Colors.FormatBold + Colors.Cyan,
		"fields": Colors.FormatBold + Colors.Magenta,
		"caller": Colors.FormatBold + Colors.Black,
	},
	UTC:    true,
	Time:   "2006-01-02 15-04-05",
	Format: nil,
}

// Work around initialization loop
func init() {
	Options.Format = DefaultConsoleFormatter
}
