# logg - The most simple & beautiful logging library for Go

## Usage

```go
package main

import "codeberg.org/momar/logg"

func main() {
	logg.Debug("This only shows up if the DEBUG environment variable is set")
	logg.Info("Hello %s", "World!")
	logg.Warn("Something went a little bit wrong")
	logg.Error("Something went " + logg.Colors.Red + "very" + logg.Colors.FormatReset + " wrong")

	l := logg.Tag("hello")
	l.Tag("world").Info("Hello World")

	l.Field("test", true).Field("whatever", false).Info("Hi")
	l.Fields(map[string]interface{}{"hello": "world"}).Info("Whatever")
}
```

![](https://codeberg.org/momar/logg/raw/branch/master/example/screenshot.png)

## Usage with [Gin](https://gin-gonic.com/)

```go
package main

import (
    "codeberg.org/momar/logg/gin"
)

func main() {
    // Enable logg
    gin.DebugPrintRouteFunc = logg4gin.DebugPrintRouteFunc
    gin.DefaultWriter = logg4gin.DefaultWriter
    gin.DefaultErrorWriter = logg4gin.DefaultWriter
    r := gin.New()
    r.Use(gin.LoggerWithFormatter(logg4gin.LogFormatter))
    
    r.Use(gin.Recovery())
    r.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
	})
	r.Run()
}
```
