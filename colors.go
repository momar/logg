package logg

var Colors = struct {
	Black   string
	Red     string
	Green   string
	Yellow  string
	Blue    string
	Magenta string
	Cyan    string
	White   string

	BlackBG   string
	RedBG     string
	GreenBG   string
	YellowBG  string
	BlueBG    string
	MagentaBG string
	CyanBG    string
	WhiteBG   string

	FormatBold      string
	FormatUnderline string
	FormatReset     string
}{
	"\033[30m",
	"\033[31m",
	"\033[32m",
	"\033[33m",
	"\033[34m",
	"\033[35m",
	"\033[36m",
	"\033[37m",

	"\033[40m",
	"\033[41m",
	"\033[42m",
	"\033[43m",
	"\033[44m",
	"\033[45m",
	"\033[46m",
	"\033[47m",

	"\033[1m",
	"\033[4m",
	"\033[0m",
}
